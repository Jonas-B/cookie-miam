<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Unit;

class EntityController extends AbstractController
{
    /**
     * @Route("/entity/{entity}/create", name="entity_create")
     */
    public function create($entity): Response
    {
        return $this->render('entity/form.html.twig', ['entity' => new Unit()]);
    }
}